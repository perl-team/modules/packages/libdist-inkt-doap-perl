libdist-inkt-doap-perl (0.110-3) unstable; urgency=medium

  * watch: rewrite usage comment
  * declare compliance with Debian Policy 4.5.0
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * stop build-depend on dh-buildinfo
  * set upstream metadata field Bug-Submit;
    remove obsolete fields Contact Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright)

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Apr 2020 18:18:52 +0200

libdist-inkt-doap-perl (0.110-2) unstable; urgency=medium

  * Simplify rules:
    + Stop resolve build-dependencies in rules file.
    + Use short-form dh sequencer (not cdbs).
      Stop build-depend on cdbs.
  * Wrap and sort control file.
  * Declare compliance with Debian Policy 4.2.1.
  * Update copyright info: Extend coverage of packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Nov 2018 13:18:41 +0100

libdist-inkt-doap-perl (0.110-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update GitHub URLs to use HTTPS.

  [ Laurent Baillet ]
  * New upstream version 0.110
  * update debian/copyright years
  * fix lintian perl-module-name-not-mentioned-in-description warning
  * add upstream metadata
  * declare compliance with Debian Policy 4.2.0
  * set test suite

 -- Laurent Baillet <laurent.baillet@gmail.com>  Sat, 25 Aug 2018 19:53:40 +0000

libdist-inkt-doap-perl (0.100-1) unstable; urgency=medium

  [ upstream ]
  New release.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to file format 4.
    + Fix track only metacpan URL.
    + Mention gbp --uscan in usage comment.
    + Use substitution strings.
  * Update copyright info:
    + Update alternative source URLs.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Modernize Vcs-* fields:
    + Use https (not git) protocol.
    + Use git (not cgit) in path.
    + Include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.2.
  * Update package relations:
    + (Build-)depend on recent perl: Needed to provide
      recent List::Util.
    + (Build-)depend on libpath-iterator-rule-perl libpath-tiny-perl
      libsoftware-license-perl libtype-tiny-perl liburi-perl
      libnamespace-autoclean-perl.
    + Relax to (build-)depend unversioned on libmoose-perl
      librdf-trin3-perl.
    + Relax to stop build-depend (explicitly) on (recent perl or)
      libtest-simple-perl: Needed version satisfied by perl even in
      oldstable.
  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
    + Resolve archs in maintainer script (not during build).
    + Stop build-depend on devscripts.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Use only Github (not also search.cpan.org) as source URL.
    + Use Github issue tracker as preferred upstream contact.
    + Stop track no longer shipped file CONTRIBUTING.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for main upstream author(s).
    + Extend coverage for myself.
  * Add lintian overrides regarding License-Reference.
  * Bump debhelper hint file to compatibility level 9.
  * Declare that normal build targets does not require root access.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Dec 2017 14:44:27 +0100

libdist-inkt-doap-perl (0.022-1) unstable; urgency=medium

  [ upstream ]
  New release.
    + Support x_IRC.
    + Include test to check if changelog data is up-to-date.
    + Use a version range string in x_breaks.

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.6.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 14 Oct 2014 11:32:17 +0200

libdist-inkt-doap-perl (0.019-1) unstable; urgency=medium

  [ upstream ]
  New release.
    + Cope better with nulls when generating CREDITS file.
    + Add a DESCRIPTION to Dist::Inkt::DOAP.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Jonas Smedegaard ]
  * Rewrite long description, based on upstream description.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 27 Aug 2014 12:27:50 +0200

libdist-inkt-doap-perl (0.018-1) unstable; urgency=medium

  [ upstream ]
  New release.
    +  - Fix don't need to load RDF::DOAP::ChangeSets.
    + Test that all roles actually compile.
    + Require Dist::Inkt 0.017.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Adjust comment for CONTRIBUTING Files section.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 13 Jun 2014 13:52:31 +0200

libdist-inkt-doap-perl (0.017-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#750180.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Jun 2014 15:04:46 +0200
